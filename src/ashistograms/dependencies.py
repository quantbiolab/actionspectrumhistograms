from .parameters import *
import scipy.sparse as spr
from scipy.sparse.linalg import eigsh
import abc

# algebra
def get_polar_vector(theta, phi, r=1):
    return r * np.array([np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)])


def get_all_eigh(M, sparse):
    if sparse:
        dim = M.shape[0]
        part_1 = eigsh(M, k=dim - 2, which='SR')
        part_2 = eigsh(M, k=2, which='LR')
        eigenvals = np.concatenate([part_1[0], part_2[0]])
        S = np.concatenate([part_1[1], part_2[1]], axis=1)
        return eigenvals, S
    else:
        return np.linalg.eigh(M)


def generate_xy_pairs(lx, ly):
    l1, l2 = np.meshgrid(ly, lx)
    return np.stack((l2.flatten(), l1.flatten()), axis=1)


# magnetism
angfreq_MHz_factor = 1 / (2 * np.pi * 1e6)


# spin operators
def get_spin_operators(spin_number):
    if '%.2f' % (spin_number) == '0.50':
        return np.array([[0, 0.5], [0.5, 0]]), np.array([[0, -0.5j], [0.5j, 0]]), (np.array([[0.5, 0], [0, -0.5]]))
    elif spin_number == 1:
        I_x = (2 ** (-0.5)) * np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]])
        I_y = (2 ** (-0.5)) * np.array([[0, -1j, 0], [1j, 0, -1j], [0, 1j, 0]])
        I_z = np.array([[1, 0, 0], [0, 0, 0], [0, 0, -1]])
        return I_x, I_y, I_z


def get_spin_number_of_dominant_isotope(element_symbol):
    spin_numbers = {'H': 1 / 2, 'N': 1}
    return spin_numbers[element_symbol]


vec_S = get_spin_operators(1 / 2)
singlet_ket = ((1 / np.sqrt(2)) * np.array([0, 1, -1, 0]))[:, np.newaxis]
electron_singlet_projection = singlet_ket @ singlet_ket.T


def electron_state(A=np.identity(2), B=np.identity(2)):
    return np.kron(A, B)


def generate_full_state_vec_S(electron, Z, num_electrons=2):
    '''For one electron, either put electron = 0 or electron =1, num_electrons = 1.'''
    if electron < 2 or num_electrons == 1:
        return [spr.kron(s, spr.identity(num_electrons * Z)) for s in vec_S]
    elif electron == 2 and num_electrons == 2:
        return [spr.kron(spr.identity(2), spr.kron(s, spr.identity(Z))) for s in vec_S]


# noinspection PyTypeChecker
def expand_spin_operator(spin_operator, ind_spin, spin_multiplicities):
    num_spins = len(spin_multiplicities)
    if ind_spin == 0:
        return spr.kron(spin_operator, spr.identity(np.prod(spin_multiplicities[1:])))
    elif ind_spin == num_spins - 1:
        return spr.kron(spr.identity(np.prod(spin_multiplicities[:-1])), spin_operator)
    else:
        before = spr.identity(np.prod(spin_multiplicities[:ind_spin]))
        after = spr.identity(np.prod(spin_multiplicities[ind_spin + 1:]))
        return spr.kron(spr.kron(before, spin_operator), after)


# Hamiltonian construction
def get_Zeeman_elecpart_from_vector(vec_omega_uT):
    vec_omega = vec_omega_uT * 1e-6 * gyro_electron
    vec_SA = [np.kron(i, np.identity(2)) for i in vec_S]
    vec_SB = [np.kron(np.identity(2), i) for i in vec_S]
    return np.sum([vec_omega[j] * (vec_SA[j] + vec_SB[j]) for j in range(3)], axis=0)


def get_Zeeman_elecpart(theta, phi, field_uT=50):
    vec_omega_uT = get_polar_vector(theta, phi, r=field_uT)
    return get_Zeeman_elecpart_from_vector(vec_omega_uT)


def get_Zeeman_elecpart_single_from_vector(electron: int, vec_omega_uT):
    vec_omega = vec_omega_uT * 1e-6 * gyro_electron
    if electron == 0:  # the only electron
        vec_self = vec_S
    elif electron == 1:  # 1st out of 2 electrons
        vec_self = [np.kron(i, np.identity(2)) for i in vec_S]
    elif electron == 2:  # 2nd out of two electrons
        vec_self = [np.kron(np.identity(2), i) for i in vec_S]
    else:
        raise ValueError(f"Electron number {electron} is not supported. Input 0/1/2.")
    return np.sum([vec_omega[j] * (vec_self[j]) for j in range(3)], axis=0)


def get_exchange_term(J_MHz, Z):
    J = -J_MHz * 1e6 * 2 * np.pi
    elecpart = np.array([[J / 2, 0, 0, 0], [0, -J / 2, J, 0], [0, J, -J / 2, 0], [0, 0, 0, J / 2]])
    return spr.csc_matrix(spr.kron(elecpart, spr.identity(Z)) + 0.5 * J * spr.identity(4 * Z))


def get_DC_term_elecpart(DC_tensor):
    return np.sum(np.array(
        [[DC_tensor[p, q] * spr.kron(vec_S[p], spr.identity(2)) @ spr.kron(spr.identity(2), vec_S[q]) for p in range(3)]
         for q in range(3)]).flatten(), axis=0).toarray()


def get_DC_term(DC_tensor, Z):
    return spr.csc_matrix(spr.kron(get_DC_term_elecpart(DC_tensor), spr.identity(Z)))


# spin systems
def determine_radical_from_input_string(user_input):
    if user_input in ['A', 'FAD', 'F']:
        return 1
    elif user_input in ['B', 'TRP', 'W']:
        return 2


def determine_element_from_atom_name(atom_name):
    if atom_name[:2] in ['WC', 'WD']:
        return atom_name[2]
    elif atom_name[0] in ['F', 'W']:
        return atom_name[1]
    else:
        return atom_name[0]


def determine_spin_number_from_atom_name(atom_name):
    element_symbol = determine_element_from_atom_name(atom_name)
    return get_spin_number_of_dominant_isotope(element_symbol)


class Nucleus:
    def __init__(self, radical, atom_name, spin_number='from_atom_name', hyperfine_tensor='ClCry_crystal'):
        if type(radical) == str:
            self.radical = determine_radical_from_input_string(radical)
        elif type(radical) == int:
            self.radical = radical
        self.name = atom_name
        self.I = self.determine_spin_number_from_input(spin_number)
        self.M = M = int(2 * self.I + 1)
        self.vec_I = get_spin_operators(self.I)
        self.coupling_is_isotropic = None
        self.aiso_MHz = None
        self.HFC_MHz = None
        self.aiso = None
        self.HFC = None
        self.determine_hyperfine_tensor_from_input(hyperfine_tensor)  # set the four attributes above
        return

    def determine_element(self):
        self.element = determine_element_from_atom_name(self.name)
        return

    def determine_spin_number_from_input(self, user_input):
        if type(user_input) != str:
            return user_input
        elif user_input == 'from_atom_name':
            return determine_spin_number_from_atom_name(self.name)

    def determine_hyperfine_tensor_from_input(self, user_input):
        if type(user_input) == str:
            self.determine_hyperfine_tensor_from_string(user_input)
        else:
            self.determine_hyperfine_tensor_from_non_string(user_input)

        if self.coupling_is_isotropic:
            self.HFC_MHz = self.aiso_MHz * np.identity(3)
        else:
            self.aiso_MHz = np.trace(self.HFC_MHz) / 3

        self.store_HFC_angfreq()
        return

    def determine_hyperfine_tensor_from_string(self, user_input):
        self.coupling_is_isotropic = user_input == 'isotropic'
        if self.coupling_is_isotropic:
            self.aiso_MHz = load_isotropic_coupling_constant(self.name)
        else:
            if self.radical == 1:
                self.HFC_MHz = FAD_HFCs_FADPA(self.name)
            elif self.radical == 2:
                self.HFC_MHz = load_tryptophan_hyperfine_coupling_tensor(user_input, self.name)

    def determine_hyperfine_tensor_from_non_string(self, user_input):
        try:  # input is a float value for the isotropic coupling constant
            self.aiso_MHz = float(user_input)
            self.coupling_is_isotropic = True
        except (TypeError, ValueError) as error:  # input is the tensor
            self.coupling_is_isotropic = False
            self.HFC_MHz = user_input
        return

    def store_HFC_angfreq(self):
        self.aiso = self.aiso_MHz * 1e6 * 2 * np.pi
        self.HFC = self.HFC_MHz * 1e6 * 2 * np.pi
        return

    def get_HFC_eigs_MHz(self):
        self.HFC_eigs_MHz = np.linalg.eigh(self.HFC_MHz)[0]
        return self.HFC_eigs_MHz

    def get_rms_HFC_eigs_MHz(self):
        self.rms_HFC_eigs_MHz = np.sqrt(np.mean([i ** 2 for i in self.get_HFC_eigs_MHz()]))
        return self.rms_HFC_eigs_MHz

    def get_a_eff_contribution_MHz2(self, isotropic=True):  # Needs reviewing
        '''(a**2)*self.I*(self.I+1) where a = (self.aiso_MHz**2) if isotropic == True and a = self.get_rms_HFC_eigs_MHz() if isotropic == False'''
        if isotropic:
            meansq_eigs_MHz = self.aiso_MHz ** 2
        else:
            meansq_eigs_MHz = self.get_rms_HFC_eigs_MHz() ** 2
        return meansq_eigs_MHz * self.I * (self.I + 1)


def generate_nuclei_from_names(names, hyperfine_tensor='ClCry_crystal'):
    return [Nucleus(name[0], name, hyperfine_tensor=hyperfine_tensor) for name in names]

def split_nuclei_by_radical(nuclei, return_spinsys=False):
    radical_1_nuclei = []
    radical_2_nuclei = []
    for nucleus in nuclei:
        if nucleus.radical == 1:
            radical_1_nuclei.append(nucleus)
        elif nucleus.radical == 2:
            radical_2_nuclei.append(nucleus)
    split_nuclei = [radical_1_nuclei, radical_2_nuclei]
    if not return_spinsys:
        return split_nuclei
    else:
        return [NuclearSpinSystem(*n) for n in split_nuclei]


class IsolatedSingleRadical:  # electron spin space only.
    def __init__(self, electron=2):
        self.Z = 1
        self.dim = 2
        self.electron = electron
        self.construct_hamiltonian = self.get_Zeeman_term
        self.full_vec_S = vec_S
        return

    def get_Zeeman_term(self, theta, phi, field_uT=50):
        vec_omega_uT = get_polar_vector(theta, phi, r=field_uT)
        return self.get_Zeeman_term_from_vector(vec_omega_uT)

    def get_Zeeman_term_from_vector(self, vec_omega_uT):
        return get_Zeeman_elecpart_single_from_vector(0, vec_omega_uT)

    def get_eigH(self, theta, phi, field_uT=50):
        H = self.construct_hamiltonian(theta, phi, field_uT)
        return np.linalg.eigh(H)


class NuclearSpinSystem:
    def __init__(self, *nuclei):
        self.nuclei = nuclei
        self.nuclei_names = [n.name for n in nuclei]
        self.name = ''.join(self.nuclei_names)
        self.num_nuclei = len(nuclei)
        self.M_values = M_values = [n.M for n in nuclei]
        self.Z = np.prod(M_values)
        self.hfcs_MHz_list = [n.HFC_MHz for n in nuclei]
        self.hfcs_list = [n.HFC for n in nuclei]
        self.aiso_MHZ_list = np.array([i.aiso_MHz for i in self.nuclei])
        return

    def nuc_state(self, nucleus_number, ind):
        return self.nuc_state_matrix(nucleus_number, self.nuclei[nucleus_number].vec_I[ind])

    def nuc_state_matrix(self, nucleus_number, mat):
        return expand_spin_operator(mat, nucleus_number, self.M_values)

    def full_state(self, nucleus_number, ind, electron_part):
        return spr.kron(electron_part, self.nuc_state(nucleus_number, ind))

    def Hamiltonian_HFC_term_of_nucleus(self, ind, num_electrons=2):
        HFC_tensor = self.hfcs_list[ind]
        rad = self.nuclei[ind].radical

        elec_dim = 2 * num_electrons
        full_vec_S = generate_full_state_vec_S(rad, self.Z, num_electrons)
        full_vec_I = [spr.kron(spr.identity(elec_dim), self.nuc_state(ind, d)) for d in range(3)]
        terms = np.array([[HFC_tensor[p][q] * (full_vec_S[p] @ full_vec_I[q]) for p in range(3)] for q in
                          range(3)]).flatten()
        return np.sum(terms, axis=0)

    def Hamiltonian_HFC_terms(self, num_electrons=2):
        return np.sum([self.Hamiltonian_HFC_term_of_nucleus(i, num_electrons) for i in range(self.num_nuclei)],
                      axis=0)

    def effective_aiso_MHz(self, factor=4 / 3):
        squared = [factor * np.sum([nuc.a_eff_contribution_MHz2() for nuc in self.nuclei if nuc.radical == ind]) for ind
                   in [1, 2]]
        return [np.sqrt(i) for i in squared]

    def construct_vec_I_tot(self):
        return np.array([np.sum([self.nuc_state(j, i) for j in range(self.num_nuclei)], axis=0) for i in range(3)])


class FullSpinSystem:
    def __init__(self, nuclear_spinsys, num_electrons):
        """A spin system containing electron and nuclear nuclear_spinsys."""
        self.spins = spins = nuclear_spinsys
        self.Z = spins.Z
        self.num_electrons = num_electrons
        self.dim = 2 * num_electrons * self.Z
        self.HFC_terms = spins.Hamiltonian_HFC_terms(num_electrons=num_electrons)
        self.identity_Z = spr.identity(self.Z)
        self.identity_full = spr.identity(self.dim)
        self.full_vec_S = None
        self.eigenspace = None
        return

    def construct_eigenspace(self, eigenvalues, S, sparse=True, transform_vecS=True):
        self.eigenspace = Eigenspace(eigenvalues, S, sparse=sparse)
        self.eigenspace.Z = self.Z  # Mainly to keep old code functional.
        if transform_vecS:
            self.eigenspace.store_eigenbasis_vec_S(self.full_vec_S)
        return

    def get_eigH(self, theta, phi, field_uT=50, sparse=True):
        H = self.construct_hamiltonian(theta, phi, field_uT)
        return get_all_eigh(H, sparse=sparse)

    @abc.abstractmethod
    def construct_hamiltonian(self, theta, phi, field_uT=50):
        ...


class SingleRadical(FullSpinSystem):
    def __init__(self, nuclear_spinsys, get_full_vec_S=True):
        FullSpinSystem.__init__(self, nuclear_spinsys, num_electrons=1)
        self.electron = int(np.unique([i.radical for i in self.spins.nuclei]))
        if get_full_vec_S:
            self.full_vec_S = self.get_full_vec_S()
        return

    def get_Zeeman_term(self, theta, phi, field_uT=50):
        vec_omega_uT = get_polar_vector(theta, phi, r=field_uT)
        return self.get_Zeeman_term_from_vector(vec_omega_uT)

    def get_Zeeman_term_from_vector(self, vec_omega_uT):
        return spr.kron(get_Zeeman_elecpart_single_from_vector(0, vec_omega_uT), spr.identity(self.Z))

    def construct_hamiltonian(self, theta, phi, field_uT=50):
        if abs(field_uT) < 1e-10:
            return self.HFC_terms
        else:
            return self.get_Zeeman_term(theta, phi, field_uT) + self.HFC_terms

    def get_full_vec_S(self):
        return generate_full_state_vec_S(self.electron, self.Z, num_electrons=self.num_electrons)


class RadicalPair(FullSpinSystem):
    def __init__(self, nuclear_spinsys, coupled=True, initial_state=None, **kwargs):
        FullSpinSystem.__init__(self, nuclear_spinsys, num_electrons=2)
        self.coupled = coupled
        if not self.coupled:
            self.J_MHz = 0
            self.DC_tensor = np.zeros((3, 3))
            self.H_nonZeeman = self.HFC_terms
        else:
            self.J_MHz = kwargs.pop('J_MHz', 0)
            self.DC_tensor = kwargs['DC_tensor']
            self.H_nonZeeman = self.HFC_terms + get_exchange_term(self.J_MHz, self.Z) \
                               + get_DC_term(self.DC_tensor, self.Z)
        self.initial_state = initial_state
        return

    def get_singlet_projection(self):
        return np.kron(electron_singlet_projection, np.identity(self.Z))

    def get_initial_state(self):
        if self.initial_state is None:
            self.initial_state = (self.get_singlet_projection() / self.Z).flatten()

    def construct_hamiltonian(self, theta, phi, field_uT=50):
        if abs(field_uT) < 1e-10:
            return self.H_nonZeeman
        else:
            return spr.kron(get_Zeeman_elecpart(theta, phi, field_uT), spr.identity(self.Z)) + self.H_nonZeeman

    def Liouvillian_H_term(self, H):
        return 1j * (spr.kron(H, spr.identity(4 * self.Z)) - spr.kron(spr.identity(4 * self.Z), H.T))


class Eigenspace:
    def __init__(self, eigenvalues, S, sparse=True, hermitian_S=True, num_electrons=None):
        """In general, assume input (e.g. S, M) is dense because any sparse matrix will just be converted into csc
        instead of throwing an error. Not ideal but works for the time being. """
        self.eigenvalues = eigenvalues
        self.use_sparse = sparse
        self.S = self.parse_dense_matrix(S)
        if hermitian_S:
            self.S_inv = np.conj(self.S).T
        else:
            self.S_inv = np.linalg.inv(S)
        self.dim = len(eigenvalues)
        self.num_electrons = num_electrons
        self.full_vec_S = None
        self.eigenbasis_vec_S = None
        self.elec_dim, self.Z = self.deduce_Z()
        return

    def parse_dense_matrix(self, matrix):
        if self.use_sparse:
            return spr.csc_matrix(matrix)
        else:
            return matrix

    def kron(self, A, B):
        if self.use_sparse:
            return spr.kron(A, B)
        else:
            return np.kron(A, B)

    def identity(self, n):
        if self.use_sparse:
            return spr.identity(n)
        else:
            return np.identity(n)

    def transform_matrix_to_eigenbasis(self, M):
        M = self.parse_dense_matrix(M)
        return self.S_inv @ M @ self.S

    def transform_electronic_part_to_eigenbasis(self, electronic_part, Z):
        return self.transform_matrix_to_eigenbasis(self.kron(electronic_part, self.identity(Z)))

    def get_eigenbasis_vec_S(self, full_vec_S):
        """Can be used to store SAqSBq as well, in which case one must be careful with naming."""
        self.full_vec_S = full_vec_S
        self.eigenbasis_vec_S = [self.S_inv @ s @ self.S for s in full_vec_S]
        return self.eigenbasis_vec_S

    def deduce_Z(self):
        if self.num_electrons is not None:
            elec_dim = 2 * self.num_electrons
            return elec_dim, int(self.dim / elec_dim)
        else:
            return None, None

    def transform_to_eigenbasis(self, M):
        """Requires self.Z to be defined. Usage not really recommended."""
        self.deduce_Z()
        if M.shape[0] == self.elec_dim:
            return self.transform_electronic_part_to_eigenbasis(M, self.Z)
        else:
            return self.transform_matrix_to_eigenbasis(M)

    @abc.abstractmethod
    def store_eigenbasis_vec_S(self, full_vec_S):
        ...
