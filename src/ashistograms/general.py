from .dependencies import *
import numpy as np
from .data_management import AnisotropicResultsLoader, generate_anisotropic_result_loaders
import abc
import gc

eigenspace_results_strings = ['_eigenvalues.npy', '_Smatrix.npy']
output_results_strings = ['_energy_gaps.npy', '_res_effects.npy', '_heights.npy', '_normalised_heights.npy']


def find_perpendicular_unit_vector(theta, phi):
    vec = get_polar_vector(theta, phi, 1)
    z_axis = np.array([0, 0, 1])
    if np.isclose(vec, z_axis).all():
        return np.array([1,0,0])
    else:
        perp = np.cross(vec, z_axis)
        return perp / np.linalg.norm(perp)


def generate_perpendicular_H_Zeeman(theta, phi, field_uT, single_electron=None):
    perpendicular_unit = find_perpendicular_unit_vector(theta, phi)
    if single_electron is None:
        return get_Zeeman_elecpart_from_vector(perpendicular_unit * field_uT)
    else:
        return get_Zeeman_elecpart_single_from_vector(single_electron, perpendicular_unit * field_uT)


class DiagonalisedSpecies:
    def __init__(self, nuclear_spinsys, path, directions=None, **kwargs):
        self.num_electrons = None
        self.nuclear_spinsys = nuclear_spinsys
        self.path = path
        self.directions = directions
        num_points = len(directions) if directions is not None else None
        self.kwargs = kwargs
        self.num_points = self.kwargs.pop('num_points', num_points)
        self.eigenvalues, self.Smats = generate_anisotropic_result_loaders(self.path, eigenspace_results_strings,
                                                                           load_all_if_exists=True,
                                                                           num_points=self.num_points)
        self.eigenspace = None

    def set_eigenspace(self, ptind):
        try:
            self.eigenvalues.set_current(ptind)
            self.Smats.set_current(ptind)
            for item in [self.eigenvalues, self.Smats]:
                if item.current.shape[0] == 1:
                    item.current = item.current[0]
        except FileNotFoundError:
            self.calculate_eigenspace(ptind)
        self.eigenspace = Eigenspace(self.eigenvalues.current, self.Smats.current, sparse=False,
                                     num_electrons=self.num_electrons)
        del self.eigenvalues.current, self.Smats.current
        return

    def get_MHz_energy_gap(self, i, j):
        return angfreq_MHz_factor * np.abs(self.eigenspace.eigenvalues[i] - self.eigenspace.eigenvalues[j])

    def calculate_eigenspace(self, ptind):
        theta, phi = self.directions[ptind]
        self.eigenvalues.current, self.Smats.current = self.get_eigH(theta, phi)
        self.eigenvalues.save_current(ptind)
        self.Smats.save_current(ptind)
        return

    @abc.abstractmethod
    def get_eigH(self, theta, phi):
        ...


class ComputedSingleRadical(DiagonalisedSpecies):
    def __init__(self, nuclear_spinsys, path, directions=None, **kwargs):
        DiagonalisedSpecies.__init__(self, nuclear_spinsys, path, directions=directions, **kwargs)
        self.num_electrons = 1
        self.radical = self.generate_radical(**kwargs)
        self.Z = self.radical.Z
        self.dim = self.radical.dim
        self.full_vec_S = self.radical.full_vec_S
        self.transition_hamiltonians = self.define_transition_hamiltonians(**self.kwargs)

    def generate_radical(self, **kwargs):
        if self.nuclear_spinsys is not None:
            return SingleRadical(self.nuclear_spinsys)
        else:
            return IsolatedSingleRadical(**kwargs)

    def define_transition_hamiltonians(self, transition_hamiltonian_path=None,
                                       transition_hamiltonian_result_string='_transition_hamiltonian.npy', **kwargs):
        if transition_hamiltonian_path is None:
            transition_hamiltonian_path = self.path
        return AnisotropicResultsLoader(transition_hamiltonian_path, transition_hamiltonian_result_string, **kwargs)

    def load_or_calculate_transition_hamiltonian(self, ptind, transition_hamiltonian):
        if self.transition_hamiltonians.result_exists(ptind):
            self.transition_hamiltonians.set_current(ptind)
        else:
            self.transition_hamiltonians.current = self.eigenspace.transform_electronic_part_to_eigenbasis(
                transition_hamiltonian, Z=self.Z)
            self.transition_hamiltonians.save_current(ptind)
        return

    def get_eigH(self, theta, phi):
        return self.radical.get_eigH(theta, phi)


class ComputedRadicalPair(DiagonalisedSpecies, abc.ABC):
    def __init__(self, nuclear_spinsys, path, directions=None, **kwargs):
        DiagonalisedSpecies.__init__(self, nuclear_spinsys, path, directions=directions, **kwargs)
        self.num_electrons = 2
        self.Z = self.nuclear_spinsys.Z
        self.dim = 4 * self.Z
        self.radical_pair = None
        self.get_eigH = None
        self.set_up_radical_pair(**kwargs)

    def set_up_radical_pair(self, **kwargs):
        try:
            self.radical_pair = self.generate_radical_pair(**kwargs)
            self.get_eigH = self.radical_pair.get_eigH
        except:
            pass  # assume not needed

    def get_eigenbasis_singlet(self):
        return self.eigenspace.transform_electronic_part_to_eigenbasis(electron_singlet_projection, self.Z)

    def generate_radical_pair(self, **kwargs):
        return RadicalPair(self.nuclear_spinsys, coupled=True, **kwargs)

    def calculate_eigenspace(self, ptind):
        theta, phi = self.directions[ptind]
        self.eigenvalues.current, self.Smats.current = self.radical_pair.get_eigH(theta, phi)
        self.eigenvalues.save_current(ptind)
        self.Smats.save_current(ptind)
        return


class HistogramCalculator:
    def __init__(self, output_path, directions, bin_edges, is_calculation_mode=True):
        self.output_path = output_path
        self.bin_edges = bin_edges
        self.directions = directions
        self.num_points = len(directions)
        self.energy_gaps, self.res_effects, self.heights, self.normalised_heights = generate_anisotropic_result_loaders(
            self.output_path, output_results_strings, load_all_if_exists=is_calculation_mode,
            num_points=self.num_points)
        if not is_calculation_mode:
            self.heights.load_all()
            self.normalised_heights.load_all()
        return

    def find_missing_ptinds(self):
        return self.heights.attempt_bundling(self.num_points)

    def normalise_all_bar_heights(self):
        if not self.normalised_heights.is_bundled:
            missing = self.find_missing_ptinds()
            if self.heights.is_bundled:
                self.normalised_heights.all_results = np.array(
                    [result / np.sum(result) for result in self.heights.all_results])
                np.save(self.normalised_heights.all_arr_path, self.normalised_heights.all_results)
            else:
                available = [i for i in range(self.num_points) if i not in missing]
                for ptind in available:
                    self.heights.set_current(ptind)
                    self.normalised_heights.current = self.heights.current / np.sum(self.heights.current)
                    self.normalised_heights.save_current(ptind)

    def calculate_if_absent(self, ptind, rf_point=None, initial_state='singlet'):
        """Returns 'exists' if the result already exists."""
        if not self.heights.result_exists(ptind):
            self.heights.current = self.calculate_bar_heights(ptind, rf_point, initial_state=initial_state)
            self.heights.save_current(ptind)
        else:
            return 'exists'

    def calculate_all(self, rf_point=None, initial_state='singlet'):
        for ptind in range(self.num_points):
            self.calculate_if_absent(ptind, rf_point=rf_point, initial_state=initial_state)
        self.normalise_all_bar_heights()
        return

    def bundle_non_height_results(self):
        for result in [self.energy_gaps, self.res_effects] + self.get_additional_non_height_results():
            result.attempt_bundling()
        return

    @abc.abstractmethod
    def set_eigenspace(self, ptind):
        ...

    @abc.abstractmethod
    def delete_eigenspace(self):
        ...

    @abc.abstractmethod
    def calculate_bar_heights(self, ptind, rf_point=None, initial_state='singlet'):
        ...

    @abc.abstractmethod
    def get_additional_non_height_results(self):
        ...


def index_and_calculate_sum_of_population_differences(populations, inds):
    return np.sum(np.abs([np.diff(populations[ind]) for ind in inds]))


def index_and_calculate_sum_of_resonance_effects(i, j, inds, populations, transition_hamiltonian):
    popdiff_sum = index_and_calculate_sum_of_population_differences(populations, inds)
    return popdiff_sum * np.abs(transition_hamiltonian[i, j]) ** 2


class UncoupledHistogramCalculator(HistogramCalculator):
    def __init__(self, computed_radicals, output_path, directions, bin_edges, is_calculation_mode=True):
        HistogramCalculator.__init__(self, output_path, directions, bin_edges=bin_edges,
                                     is_calculation_mode=is_calculation_mode)
        self.radA, self.radB = self.computed_radicals = computed_radicals
        self.populations = AnisotropicResultsLoader(self.output_path, '_populations.npy', num_points=self.num_points)
        return

    def set_eigenspace(self, ptind):
        for rad in self.computed_radicals:
            rad.set_eigenspace(ptind)
            rad.eigenspace.get_eigenbasis_vec_S(full_vec_S=rad.full_vec_S)
        return

    def delete_eigenspace(self):
        del self.radA.eigenspace, self.radB.eigenspace
        return

    def calculate_bar_heights(self, ptind, rf_point=None, initial_state='singlet'):
        if self.res_effects.result_exists(ptind):
            self.energy_gaps.set_current(ptind)
            self.res_effects.set_current(ptind)
        else:
            self.set_eigenspace(ptind)
            self.energy_gaps.current, self.res_effects.current = self.calculate_transitions(ptind, rf_point=rf_point,
                                                                                            initial_state=initial_state)
            self.energy_gaps.save_current(ptind)
            self.res_effects.save_current(ptind)
            self.delete_eigenspace()
        return extract_AS_histogram_height(self.energy_gaps.current, self.res_effects.current,
                                           self.bin_edges)

    def calculate_transitions(self, ptind, rf_point=None, initial_state='singlet'):
        if self.populations.result_exists(ptind):
            self.populations.set_current(ptind)
        else:
            self.populations.current = self.calculate_populations(initial_state)
            self.populations.save_current(ptind)

        electronic_transition_hamiltonian = self.calculate_electronic_transition_hamiltonian(ptind, rf_point=rf_point)
        for rad in self.computed_radicals:
            rad.load_or_calculate_transition_hamiltonian(ptind, electronic_transition_hamiltonian)

        all_transitions = (self.calculate_radA_transitions() + self.calculate_radB_transitions())
        del self.populations.current
        del self.radA.transition_hamiltonians.current, self.radB.transition_hamiltonians.current
        return np.array(all_transitions).T

    def calculate_populations(self, initial_state='singlet'):
        def get_Sq_diagonal(eigenspace, q):
            return eigenspace.eigenbasis_vec_S[q].diagonal()

        def get_SAqkronSBq_diagonal(q):
            return diagonal_of_kronecker_product(get_Sq_diagonal(self.radA.eigenspace, q),
                                                 get_Sq_diagonal(self.radB.eigenspace, q))

        if initial_state == 'singlet':
            return 0.25 - np.real(np.sum([get_SAqkronSBq_diagonal(q) for q in range(3)], axis=0))
        elif initial_state == 'triplet':
            return 0.75 + np.real(np.sum([get_SAqkronSBq_diagonal(q) for q in range(3)], axis=0))
        else:
            raise NotImplementedError('States other than "singlet" and "triplet" are not supported yet.')

    def calculate_electronic_transition_hamiltonian(self, ptind=None, rf_point=None):
        if rf_point is None:
            theta, phi = self.directions[ptind]
            return generate_perpendicular_H_Zeeman(theta, phi, field_uT=0.01, single_electron=0)
        else:
            vec_omega_uT = get_polar_vector(theta=rf_point[0], phi=rf_point[1], r=0.01)
            return get_Zeeman_elecpart_single_from_vector(electron=0, vec_omega_uT=vec_omega_uT)

    def calculate_radA_transitions(self):
        transitions = []
        for i in range(self.radA.dim):
            for j in range(i + 1, self.radA.dim):
                inds = np.array(
                    [self.radB.dim * i + np.arange(self.radB.dim), self.radB.dim * j + np.arange(self.radB.dim)]).T
                reseffects_sum = index_and_calculate_sum_of_resonance_effects(i, j, inds, self.populations.current,
                                                                              self.radA.transition_hamiltonians.current)
                transitions.append([self.radA.get_MHz_energy_gap(i, j), reseffects_sum])
        return transitions

    def calculate_radB_transitions(self):
        transitions = []
        for i in range(self.radB.dim):
            for j in range(i + 1, self.radB.dim):
                inds = np.array(
                    [i + self.radB.dim * np.arange(self.radA.dim), j + self.radB.dim * np.arange(self.radA.dim)]).T
                reseffects_sum = index_and_calculate_sum_of_resonance_effects(i, j, inds, self.populations.current,
                                                                              self.radB.transition_hamiltonians.current)
                transitions.append([self.radB.get_MHz_energy_gap(i, j), reseffects_sum])
        return transitions

    def get_additional_non_height_results(self):
        return [self.populations]


class CoupledHistogramCalculator(HistogramCalculator):
    def __init__(self, radical_pair, output_path, directions, bin_edges, is_calculation_mode=True):
        HistogramCalculator.__init__(self, output_path, directions, bin_edges=bin_edges,
                                     is_calculation_mode=is_calculation_mode)
        self.rp = radical_pair
        self.energy_gaps_calculated = None
        self.res_effects_calculated = None
        self.populations = AnisotropicResultsLoader(self.output_path, '_populations.npy', num_points=self.num_points)
        self.transition_probabilities = AnisotropicResultsLoader(self.output_path, '_transition_probabilities.npy',
                                                                 num_points=self.num_points)
        return

    def set_eigenspace(self, ptind):
        self.rp.set_eigenspace(ptind)
        return

    def delete_eigenspace(self):
        del self.rp.eigenspace
        return

    def calculate_transition_hamiltonian(self, ptind=None, rf_point=None):
        if rf_point is None:
            theta, phi = self.directions[ptind]
            return generate_perpendicular_H_Zeeman(theta, phi, field_uT=0.01)
        else:
            return get_Zeeman_elecpart(theta=rf_point[0], phi=rf_point[1], field_uT=0.01)

    def identify_missing_quantities(self, ptind):
        self.energy_gaps_calculated = self.energy_gaps.result_exists(ptind)
        self.res_effects_calculated = self.res_effects.result_exists(ptind)
        return

    def calculate_bar_heights(self, ptind, rf_point=None, initial_state='singlet'):
        self.identify_missing_quantities(ptind)
        if self.energy_gaps_calculated and self.res_effects_calculated:
            self.load_calculated_quantities(ptind)
        else:
            self.calculate_and_store_missing_quantities(ptind, rf_point=rf_point, initial_state=initial_state)
            self.load_calculated_quantities(ptind)
        return extract_AS_histogram_height(self.energy_gaps.current, self.res_effects.current,
                                           self.bin_edges)

    def only_calculate_and_store_energy_gaps(self, ptind):
        eigenvalues = self.rp.eigenvalues.load_ptind(ptind)
        self.energy_gaps.current = angfreq_MHz_factor * calculate_all_energy_gaps(eigenvalues)
        self.energy_gaps.save_current(ptind)
        return

    def only_calculate_and_store_res_effects(self, ptind, rf_point=None):
        self.set_eigenspace(ptind)
        del self.rp.eigenspace.eigenvalues
        self.res_effects.current = self.calculate_res_effects(ptind, rf_point=rf_point)
        self.res_effects.save_current(ptind)
        self.delete_eigenspace()
        return

    def calculate_and_store_missing_quantities(self, ptind, rf_point=None, initial_state='singlet'):
        if self.res_effects_calculated:
            self.only_calculate_and_store_energy_gaps(ptind)
        elif self.energy_gaps_calculated:
            self.only_calculate_and_store_res_effects(ptind, rf_point=rf_point)
        else:
            self.calculate_transitions_and_store_results(ptind, rf_point=rf_point, initial_state=initial_state)
        gc.collect()
        return

    def calculate_transitions_and_store_results(self, ptind, rf_point=None, initial_state='singlet'):
        self.set_eigenspace(ptind)
        self.res_effects.current = self.calculate_res_effects(ptind, rf_point=rf_point, initial_state=initial_state)
        self.res_effects.save_current(ptind)
        gc.collect()
        self.energy_gaps.current = angfreq_MHz_factor * calculate_all_energy_gaps(self.rp.eigenspace.eigenvalues)
        self.energy_gaps.save_current(ptind)
        self.delete_eigenspace()
        return

    def delete_Smats_on_condition(self, condition):
        if condition:
            del self.rp.eigenspace.S_inv, self.rp.eigenspace.S

    def calculate_res_effects(self, ptind, rf_point=None, initial_state='singlet'):
        populations_calculated = self.populations.result_exists(ptind)
        transition_probabilities_calculated = self.transition_probabilities.result_exists(ptind)
        self.delete_Smats_on_condition(populations_calculated and transition_probabilities_calculated)

        if not populations_calculated:
            self.populations.current = self.calculate_populations(initial_state=initial_state)
            self.populations.save_current(ptind)
            self.delete_Smats_on_condition(transition_probabilities_calculated)

        if not transition_probabilities_calculated:
            transition_hamiltonian = self.calculate_transition_hamiltonian(ptind, rf_point)
            eigenbasis_transition_hamiltonian = self.rp.eigenspace.transform_electronic_part_to_eigenbasis(
                transition_hamiltonian, self.rp.Z)
            self.delete_Smats_on_condition(True)
            self.transition_probabilities.current = []
            for i in range(self.rp.dim):
                for j in range(i + 1, self.rp.dim):
                    transprob = np.square(np.abs(eigenbasis_transition_hamiltonian[i, j]))
                    self.transition_probabilities.current.append(transprob)
            del eigenbasis_transition_hamiltonian
            self.transition_probabilities.save_current(ptind)

        if populations_calculated:
            try:
                population_differences = np.load(
                    self.populations.generate_ptind_dest(ptind).replace('_populations.npy', '_pop_diffs.npy'))
            except FileNotFoundError:
                self.populations.set_current(ptind)
                population_differences = calculate_all_population_differences(self.populations.current)
        else:
            population_differences = calculate_all_population_differences(self.populations.current)

        if transition_probabilities_calculated:
            self.transition_probabilities.set_current(ptind)

        return population_differences * self.transition_probabilities.current

    def calculate_populations(self, initial_state='singlet'):
        singlet_populations = np.diag(self.rp.get_eigenbasis_singlet())
        if initial_state == 'singlet':
            return singlet_populations
        elif initial_state == 'triplet':
            return 1 - singlet_populations
        else:
            raise NotImplementedError('States other than "singlet" and "triplet" are not supported yet.')

    def load_calculated_quantities(self, ptind):
        if self.energy_gaps_calculated:
            self.energy_gaps.set_current(ptind)
        if self.res_effects_calculated:
            self.res_effects.set_current(ptind)
        return

    def get_additional_non_height_results(self):
        return [self.populations, self.transition_probabilities]


class SingleRadicalHistogramCalculator(HistogramCalculator):
    def __init__(self, computed_radical, output_path, directions, bin_edges, is_calculation_mode=True):
        """Like the AZ radical_name pair, but the transitions in Z are entirely ignored."""
        HistogramCalculator.__init__(self, output_path, directions, bin_edges=bin_edges,
                                     is_calculation_mode=is_calculation_mode)
        self.rad = computed_radical
        self.populations = AnisotropicResultsLoader(self.output_path, '_populations.npy', num_points=self.num_points)
        return

    def set_eigenspace(self, ptind):
        self.rad.set_eigenspace(ptind)
        self.rad.eigenspace.get_eigenbasis_vec_S(full_vec_S=self.rad.full_vec_S)
        return

    def delete_eigenspace(self):
        del self.rad.eigenspace
        return

    def calculate_bar_heights(self, ptind, rf_point=None, initial_state='singlet'):
        if self.res_effects.result_exists(ptind):
            self.energy_gaps.set_current(ptind)
            self.res_effects.set_current(ptind)
        else:
            self.set_eigenspace(ptind)
            self.energy_gaps.current, self.res_effects.current = self.calculate_transitions(ptind, rf_point=rf_point,
                                                                                            initial_state=initial_state)
            self.energy_gaps.save_current(ptind)
            self.res_effects.save_current(ptind)
            self.delete_eigenspace()
        return extract_AS_histogram_height(self.energy_gaps.current, self.res_effects.current,
                                           self.bin_edges)

    def calculate_populations(self, initial_state='singlet'):
        def get_Sq_diagonal(eigenspace, q):
            return eigenspace.eigenbasis_vec_S[q].diagonal()

        def get_SAqkronSBq_diagonal(q):
            return diagonal_of_kronecker_product(get_Sq_diagonal(self.rad.eigenspace, q), np.diag(vec_S[q]))

        if initial_state == 'singlet':
            return 0.25 - np.real(np.sum([get_SAqkronSBq_diagonal(q) for q in range(3)], axis=0))
        elif initial_state == 'triplet':
            return 0.75 + np.real(np.sum([get_SAqkronSBq_diagonal(q) for q in range(3)], axis=0))
        else:
            raise NotImplementedError('States other than "singlet" and "triplet" are not supported yet.')

    def calculate_transition_hamiltonian(self, ptind=None, rf_point=None):
        if rf_point is None:
            theta, phi = self.directions[ptind]
            return generate_perpendicular_H_Zeeman(theta, phi, field_uT=0.01, single_electron=0)
        else:
            vec_omega_uT = get_polar_vector(theta=rf_point[0], phi=rf_point[1], r=0.01)
            return get_Zeeman_elecpart_single_from_vector(electron=0, vec_omega_uT=vec_omega_uT)

    def calculate_rad_transitions(self):
        transitions = []
        for i in range(self.rad.dim):
            for j in range(i + 1, self.rad.dim):
                inds = np.array([2 * i + np.arange(2), 2 * j + np.arange(2)]).T
                reseffects_sum = index_and_calculate_sum_of_resonance_effects(i, j, inds, self.populations.current,
                                                                              self.rad.transition_hamiltonians.current)
                transitions.append([self.rad.get_MHz_energy_gap(i, j), reseffects_sum])
        return transitions

    def calculate_transitions(self, ptind, rf_point=None, initial_state='singlet'):
        if self.populations.result_exists(ptind):
            self.populations.set_current(ptind)
        else:
            self.populations.current = self.calculate_populations(initial_state=initial_state)
            self.populations.save_current(ptind)

        transition_hamiltonian = self.calculate_transition_hamiltonian(ptind, rf_point=rf_point)
        self.rad.load_or_calculate_transition_hamiltonian(ptind, transition_hamiltonian)

        all_transitions = self.calculate_rad_transitions()
        del self.populations.current, self.rad.transition_hamiltonians.current
        return np.array(all_transitions).T

    def get_additional_non_height_results(self):
        return [self.populations]


def calculate_unnormalised_bar_height_components(own_vector, other_vector, transition_hamiltonian, singlet_projection):
    transition_braket = np.conj(own_vector) @ transition_hamiltonian @ other_vector
    transition_probability = np.abs(transition_braket) ** 2
    population_difference = np.abs(np.conj(own_vector) @ singlet_projection @ own_vector - np.conj(
        other_vector) @ singlet_projection @ other_vector)
    return transition_probability, population_difference


def calculate_AS_histogram_parameters_productbasis(eigenvalues, eigenvectors, transition_hamiltonian,
                                                   singlet_projection):
    dim = len(eigenvalues)
    outputs = []
    for j in range(dim - 1):
        own_val = eigenvalues[j]
        own_vector = eigenvectors[j]
        energy_gaps = [abs(other_val - own_val) for other_val in eigenvalues[j + 1:]]
        components = [
            calculate_unnormalised_bar_height_components(own_vector, other_vector, transition_hamiltonian,
                                                         singlet_projection)
            for other_vector in eigenvectors[j + 1:]]
        outputs.append(np.array([[energy_gaps[v]] + list(components[v]) for v in range(dim - j - 1)]))
    return np.concatenate(outputs).T


def extract_AS_histogram_height_old(arr_energy_gaps_MHz, arr_resonance_effects, bin_edges):
    def extract_height(arr_effects, arr_eiginds):
        if len(arr_eiginds) != 0:
            return np.sum(arr_effects[arr_eiginds])
        else:
            return 0

    eiginds_ascending = list(np.argsort(arr_energy_gaps_MHz))
    binned_eiginds = []
    start = 0
    for edge in bin_edges[1:]:
        unbinned_eiginds = eiginds_ascending[start:]
        unbinned_gaps = arr_energy_gaps_MHz[unbinned_eiginds]
        mask = unbinned_gaps < edge
        eiginds_in_bin = np.array(unbinned_eiginds)[mask]
        start = start + len(eiginds_in_bin)
        binned_eiginds.append(eiginds_in_bin)
    return np.array([extract_height(np.abs(arr_resonance_effects), inds) for inds in binned_eiginds])


def extract_AS_histogram_height(arr_energy_gaps_MHz, arr_resonance_effects, bin_edges):
    return np.histogram(arr_energy_gaps_MHz, bins=bin_edges, weights=arr_resonance_effects)[0]


def find_rightmost_bin_number(bar_heights, zero_margin=1e-15):
    hind = len(bar_heights) - 1
    while bar_heights[hind] < zero_margin:
        hind -= 1
    return hind


def get_cutoff_frequency(bin_midpoints, bar_heights, zero_margin=1e-15):
    return bin_midpoints[find_rightmost_bin_number(bar_heights, zero_margin=zero_margin)]


def get_nth_percentile(n, bin_midpoints, bar_heights):
    return bin_midpoints[np.searchsorted(np.cumsum(bar_heights), n / 100)]


# Used in the RF field effects project
def get_full_eigenvalues(arr_eigenvaluesA, arr_eigenvaluesB):
    return np.sum(generate_xy_pairs(arr_eigenvaluesA, arr_eigenvaluesB), axis=1)


def diagonal_of_kronecker_product(diag1, diag2):
    """Computes diagonal of the kronecker product of M1 and M2 using the diagonals of M1 and M2 as input"""
    zero_margin = 1e-15
    if np.max(np.abs(np.imag(diag1))) < zero_margin:  # likely some real physical value
        diag1 = np.real(diag1)
    if np.max(np.abs(np.imag(diag2))) < zero_margin:  # likely some real physical value
        diag2 = np.real(diag2)
    return np.concatenate([i * diag2 for i in diag1])


def calculate_all_energy_gaps(energies):
    dim = len(energies)
    return np.concatenate([np.abs(energies[eigind + 1:] - energies[eigind]) for eigind in range(dim - 1)])


def calculate_all_population_differences(populations):
    dim = len(populations)
    all_results = []
    for eigind in range(dim - 1):
        result_row = populations[eigind + 1:] - populations[eigind]
        for result in result_row:
            all_results.append(np.abs(result))
    return np.array(all_results)
    # return np.concatenate([np.abs(populations[eigind + 1:] - populations[eigind]) for eigind in range(dim - 1)])


def extract_all_transition_probabilities_eigenbasis(eigbasis_H_perp):
    #     This is the worst code I have ever written:
    dim = eigbasis_H_perp.shape[0]
    try:
        eigbasis_H_perp = eigbasis_H_perp.toarray()
    except AttributeError:
        pass
    AIJ = [eigbasis_H_perp[eigind, eigind + 1:].flatten() for eigind in range(dim - 1)]
    return np.array([np.abs(element) ** 2 for element in np.concatenate(AIJ)])


def single_radical_eigenspace(radical, eigenvalues, S, transform_vecS=True):
    """Only exists to keep old code running"""
    radical.construct_eigenspace(eigenvalues, S, transform_vecS=transform_vecS)
    return radical.eigenspace


def generate_summary_bin_column(bin_edges):
    num_bins = len(bin_edges) - 1
    return [f'{bin_edges[i]:.2f} - {bin_edges[i + 1]:.2f}' for i in range(num_bins)]
