from .dependencies import *
from .general import ComputedSingleRadical, ComputedRadicalPair, SingleRadicalHistogramCalculator, CoupledHistogramCalculator, UncoupledHistogramCalculator
import os
import numpy as np
import abc

atom_list = ['FN5', 'WCNE1', 'WCHE1', 'FN10', 'WCHB1', 'WCHE3', 'FH82', 'FH83', 'FH81', "FH1'", "FH1''", 'FH6',
             'WCHZ2', 'WCHD1', 'WCHH2', 'WCN', 'FH73', 'FH72', 'FH71', 'WCHA', 'FH9', 'WCHB2', 'WCHZ3', 'FN3', 'WCHNm',
             'FH3', 'FN1']
F_atom_list = [n for n in atom_list if n[0] == 'F']
W_atom_list = [n for n in atom_list if n[0] == 'W']

bar_width = 0.1
bar_kwargs = {'width': bar_width, 'facecolor': 'mediumseagreen'}
bin_edges = np.arange(0, 240.01, bar_width)


def generate_spin_system_name(nuclear_spinsys):
    name_of_nucleus_added = nuclear_spinsys.nuclei_names[-1]
    return '%02d_nuc_%s_added' % (nuclear_spinsys.num_nuclei, name_of_nucleus_added)


def assign_spin_system_name(nuclear_spinsys):
    nuclear_spinsys.name = generate_spin_system_name(nuclear_spinsys)
    return nuclear_spinsys


def generate_spinsys_from_names(nuclei_names, assign_default_name=True):
    if assign_default_name:
        return assign_spin_system_name(NuclearSpinSystem(*generate_nuclei_from_names(nuclei_names)))
    else:
        return NuclearSpinSystem(*generate_nuclei_from_names(nuclei_names))


def generate_nuclear_spinsys(num_nuclei, assign_default_name=True):
    nuclei_names = atom_list[:num_nuclei]
    return generate_spinsys_from_names(nuclei_names, assign_default_name=assign_default_name)


def generate_FAD_nuclear_spinsys(num_FAD_nuclei, assign_default_name=True):
    nuclei_names = F_atom_list[:num_FAD_nuclei]
    return generate_spinsys_from_names(nuclei_names, assign_default_name=assign_default_name)


def generate_W_nuclear_spinsys(num_W_nuclei, assign_default_name=True):
    nuclei_names = W_atom_list[:num_W_nuclei]
    return generate_spinsys_from_names(nuclei_names, assign_default_name=assign_default_name)


def generate_individual_radical_nucspins_from_names(nuclei_names):
    nuclei = generate_nuclei_from_names(nuclei_names)
    nuc_spin_systems = [NuclearSpinSystem(*nucs) for nucs in split_nuclei_by_radical(nuclei)]
    return [assign_spin_system_name(spinsys) for spinsys in nuc_spin_systems]


def generate_calculator_for_uncoupled_FW(nuclear_spinsys, radical_eig_directories, directions, output_path=None, calc_mode=True):
    generator = UncoupledFWCalculatorGenerator(nuclear_spinsys, radical_eig_directories, directions,
                                               output_path=output_path)
    calculator = generator.generate_calculator(calc_mode)
    del generator
    return calculator


def generate_calculator_for_uncoupled_AZ(nuclear_spinsys, radical_eig_directories, directions, output_path=None, calc_mode=True):
    """A can be either FAD or W."""
    generator = UncoupledAZCalculatorGenerator(nuclear_spinsys, radical_eig_directories, directions,
                                               output_path=output_path)
    calculator = generator.generate_calculator(calc_mode)
    del generator
    return calculator


def generate_calculator_for_coupled_radical_pair(nuclear_spinsys, directions, rp_eig_directory=None, output_path=None, calc_mode=True):
    """Assumes that the radical_name pair eigenspace has been already computed."""
    generator = CoupledCalculatorGenerator(nuclear_spinsys, directions, rp_eig_directory=rp_eig_directory,
                                           output_path=output_path)
    calculator = generator.generate_calculator(calc_mode)
    del generator
    return calculator


def generate_calculator_for_single_radical(nuclear_spinsys, radical_eig_directory, directions =None, output_path=None, calc_mode=True):
    """Like the AZ radical_name pair, but the transitions in Z are entirely ignored."""
    generator = SingleRadicalCalculatorGenerator(nuclear_spinsys, radical_eig_directory=radical_eig_directory, directions= directions, output_path=output_path)
    calculator = generator.generate_calculator(calc_mode)
    del generator
    return calculator


class CalculatorGenerator:
    def __init__(self, nuclear_spinsys, directions, output_path=None):
        self.nucsys = nuclear_spinsys
        self.nuclei_names = nuclear_spinsys.nuclei_names
        self.directions = directions
        self.num_points = len(directions)
        self.output_path = self.parse_output_path_input(output_path)
        os.makedirs(self.output_path, exist_ok=True)
        return

    def parse_output_path_input(self, user_input):
        return user_input if user_input is not None else generate_spin_system_name(self.nucsys)

    @abc.abstractmethod
    def generate_calculator(self, is_calculation_mode=True):
        ...


class CoupledCalculatorGenerator(CalculatorGenerator):
    def __init__(self, nuclear_spinsys, directions, rp_eig_directory=None, output_path=None):
        CalculatorGenerator.__init__(self, nuclear_spinsys, directions, output_path)
        if rp_eig_directory is None:
            rp_eig_directory = self.nucsys.name
        self.radical_pair = ComputedRadicalPair(self.nucsys, rp_eig_directory, directions=self.directions)
        return

    def generate_calculator(self, is_calculation_mode = True):
        return CoupledHistogramCalculator(self.radical_pair, self.output_path, self.directions, bin_edges=bin_edges, is_calculation_mode=is_calculation_mode)


class UncoupledCalculatorGenerator(CalculatorGenerator):
    def __init__(self, nuclear_spinsys, radical_eig_directories, directions, output_path=None):
        CalculatorGenerator.__init__(self, nuclear_spinsys, directions, output_path)
        self.computed_radicals = self.construct_computed_radicals(radical_eig_directories)
        return

    def generate_calculator(self, is_calculation_mode=True):
        return UncoupledHistogramCalculator(self.computed_radicals, self.output_path, self.directions,
                                            bin_edges=bin_edges, is_calculation_mode=is_calculation_mode)

    @abc.abstractmethod
    def construct_computed_radicals(self, radical_eig_directories):
        ...


class UncoupledFWCalculatorGenerator(UncoupledCalculatorGenerator):
    def construct_computed_radicals(self, radical_eig_directories):
        FAD_directory, W_directory = radical_eig_directories
        FAD_nucsys, W_nucsys = generate_individual_radical_nucspins_from_names(self.nuclei_names)
        FAD = ComputedSingleRadical(FAD_nucsys, FAD_directory + FAD_nucsys.name, directions=self.directions)
        W = ComputedSingleRadical(W_nucsys, W_directory + W_nucsys.name, directions=self.directions)
        return [FAD, W]


class UncoupledAZCalculatorGenerator(UncoupledCalculatorGenerator):
    def construct_computed_radicals(self, radical_eig_directories):
        A_directory, Z_directory = radical_eig_directories
        A_nucsys = self.nucsys
        A = ComputedSingleRadical(A_nucsys, A_directory + A_nucsys.name, directions=self.directions)
        Z = ComputedSingleRadical(None, Z_directory, directions=self.directions)
        return [A, Z]


class SingleRadicalCalculatorGenerator(CalculatorGenerator):
    def __init__(self, nuclear_spinsys, radical_eig_directory=None, directions=None, output_path=None):
        """Like the AZ radical_name pair, but the transitions in Z are entirely ignored."""
        CalculatorGenerator.__init__(self, nuclear_spinsys, directions, output_path)
        self.radical = ComputedSingleRadical(self.nucsys, radical_eig_directory+self.nucsys.name, directions=self.directions)
        return

    def generate_calculator(self, is_calculation_mode=True):
        return SingleRadicalHistogramCalculator(self.radical, self.output_path, self.directions, bin_edges=bin_edges, is_calculation_mode=is_calculation_mode)