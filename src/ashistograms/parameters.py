import numpy as np
import pandas as pd
import scipy.constants

gyro_electron = abs(scipy.constants.physical_constants['electron gyromag. ratio'][0])


def mT_to_MHz(val_mT):
    return val_mT * 1e-3 * scipy.constants.physical_constants['electron gyromag. ratio over 2 pi'][0]


def MHz_to_mT(value):
    return 1e3 * (value / scipy.constants.physical_constants['electron gyromag. ratio over 2 pi'][0])


Gaussian2004_FAD_rotation_matrix = np.array(
    [[0.8603464852717276, -0.5097096480159905, 0.0], [0.5097096480159905, 0.8603464852717276, 0.0], [0.0, 0.0, 1.0]])
Gaussian2004_W_rotation_matrix = np.array([[-0.6285147275607671, 0.7655969456514179, 0.13722446592512128],
                                           [0.7349868475114238, 0.642329156028544, -0.2172730754163464],
                                           [-0.2544868782951958, -0.035701150189851295, -0.9664170200543283]])

Gaussian2004_iso_MHz = dict()
# Commonly used ones where the F/W prefix can be skipped
Gaussian2004_iso_MHz['N5'] = 14.66545
Gaussian2004_iso_MHz['N10'] = 5.28776
Gaussian2004_iso_MHz['NE1'] = 9.01091
Gaussian2004_iso_MHz['HE1'] = -16.76857

Gaussian2004_iso_MHz["FN5"] = 14.66545
Gaussian2004_iso_MHz["FN10"] = 5.28776
Gaussian2004_iso_MHz["FN1"] = -0.09682
Gaussian2004_iso_MHz["FN3"] = -1.07389
Gaussian2004_iso_MHz["FH3"] = -0.52839
Gaussian2004_iso_MHz["FH1'"] = 11.40703
Gaussian2004_iso_MHz["FH1''"] = 11.40703
Gaussian2004_iso_MHz["FH9"] = 1.58379
Gaussian2004_iso_MHz["FH81"] = 12.32934
Gaussian2004_iso_MHz["FH82"] = 12.32934
Gaussian2004_iso_MHz["FH83"] = 12.32934
Gaussian2004_iso_MHz["FH71"] = -3.96765
Gaussian2004_iso_MHz["FH72"] = -3.96765
Gaussian2004_iso_MHz["FH73"] = -3.96765
Gaussian2004_iso_MHz["FH6"] = -10.85125
Gaussian2004_iso_MHz["WNE1"] = 9.01091
Gaussian2004_iso_MHz["WHE1"] = -16.76857
Gaussian2004_iso_MHz["WHD1"] = -7.79138
Gaussian2004_iso_MHz["WHZ2"] = -10.19254
Gaussian2004_iso_MHz["WHH2"] = -5.83710
Gaussian2004_iso_MHz["WHZ3"] = -1.12131
Gaussian2004_iso_MHz["WHE3"] = -13.67631
Gaussian2004_iso_MHz["WHB1"] = 44.96815
Gaussian2004_iso_MHz["WHB2"] = 1.28070
Gaussian2004_iso_MHz["WHA"] = -2.60835
Gaussian2004_iso_MHz["WHNa"] = -0.29119
Gaussian2004_iso_MHz["WHNb"] = 0.65336
Gaussian2004_iso_MHz["WHNm"] = (-0.29119 + 0.65336) / 2  # I think?
Gaussian2004_iso_MHz["WN"] = 4.10515
Gaussian2004_iso_MHz["WHXT"] = -1.11090


def Gaussian2004_iso_MHz_dataframe():
    df = pd.DataFrame([i for i in Gaussian2004_iso_MHz.keys() if i[0] == 'F' or i[0] == 'W'], columns=['atom_name'])
    df['radical_name'] = [i[0] for i in df['atom_name']]
    df['aiso_MHz'] = [Gaussian2004_iso_MHz[i] for i in df['atom_name']]
    df['aiso_mT'] = [MHz_to_mT(i) for i in df['aiso_MHz']]
    df['abs(aiso_MHz)'] = abs(df['aiso_MHz'])
    return df


FAD_hyperfine_available = ['N5', 'N10'] + [i for i in Gaussian2004_iso_MHz.keys() if i[0] == 'F']
W_hyperfine_available = ['NE1', 'HE1'] + [i for i in Gaussian2004_iso_MHz.keys() if i[0] == 'W']

Gaussian2004_anis_MHz = dict()
Gaussian2004_anis_MHz['N5'] = np.array([-17.472, -17.099, 34.571])
Gaussian2004_anis_MHz['N10'] = np.array([-5.965, -5.691, 11.656])
Gaussian2004_anis_MHz['NE1'] = np.array([-10.793, -10.496, 21.288])
Gaussian2004_anis_MHz['HE1'] = np.array([-13.571, -3.001, 16.573])

Gaussian2004_anis_MHz["FN5"] = np.array([-17.472, -17.099, 34.571])
Gaussian2004_anis_MHz["FN10"] = np.array([-5.965, -5.691, 11.656])
Gaussian2004_anis_MHz["FN1"] = np.array([-0.667, -0.495, 1.162])
Gaussian2004_anis_MHz["FN3"] = np.array([-0.135, -0.013, 0.149])
Gaussian2004_anis_MHz["FH3"] = np.array([-1.280, -0.317, 1.596])
Gaussian2004_anis_MHz["FH1'"] = np.array([0., 0., 0.])
Gaussian2004_anis_MHz["FH1''"] = np.array([0., 0., 0.])
Gaussian2004_anis_MHz["FH9"] = np.array([-1.723, -0.045, 1.768])
Gaussian2004_anis_MHz["FH81"] = np.array([0., 0., 0.])
Gaussian2004_anis_MHz["FH82"] = np.array([0., 0., 0.])
Gaussian2004_anis_MHz["FH83"] = np.array([0., 0., 0.])
Gaussian2004_anis_MHz["FH71"] = np.array([0., 0., 0.])  # np.array([-0.745,-0.320,1.065])
Gaussian2004_anis_MHz["FH72"] = np.array([0., 0., 0.])  # np.array([-0.745,-0.320,1.065])
Gaussian2004_anis_MHz["FH73"] = np.array([0., 0., 0.])  # np.array([-0.773,-0.567,1.340])
Gaussian2004_anis_MHz["FH6"] = np.array([-4.014, -1.300, 5.314])
Gaussian2004_anis_MHz["WNE1"] = np.array([-10.793, -10.496, 21.288])
Gaussian2004_anis_MHz["WHE1"] = np.array([-13.571, -3.001, 16.573])
Gaussian2004_anis_MHz["WHD1"] = np.array([-5.426, -2.575, 8.001])
Gaussian2004_anis_MHz["WHZ2"] = np.array([-5.452, -1.665, 7.117])
Gaussian2004_anis_MHz["WHH2"] = np.array([-4.161, -1.385, 5.546])
Gaussian2004_anis_MHz["WHZ3"] = np.array([-1.771, -1.727, 3.498])
Gaussian2004_anis_MHz["WHE3"] = np.array([-7.064, -1.346, 8.410])
Gaussian2004_anis_MHz["WHB1"] = np.array([-2.986, -1.277, 4.262])
Gaussian2004_anis_MHz["WHB2"] = np.array([-2.124, -1.270, 3.393])
Gaussian2004_anis_MHz["WHA"] = np.array([-3.060, 1.105, 1.955])
Gaussian2004_anis_MHz["WHNa"] = np.array([-2.197, -0.709, 2.906])
Gaussian2004_anis_MHz["WHNb"] = np.array([-1.733, -0.392, 2.125])
Gaussian2004_anis_MHz["WN"] = np.array([-0.627, -0.581, 1.208])
Gaussian2004_anis_MHz["WHXT"] = np.array([-1.696, 0.651, 1.045])


def Gaussian2004_anis_MHz_dataframe():
    df = pd.DataFrame([i for i in Gaussian2004_anis_MHz.keys() if i[0] == 'F' or i[0] == 'W'], columns=['atom_name'])
    df['radical_name'] = [i[0] for i in df['atom_name']]
    cols_T = ['T1_MHz', 'T2_MHz', 'T3_MHz']
    df[cols_T] = [Gaussian2004_anis_MHz[i] for i in df['atom_name']]
    df['anis_norm'] = [np.linalg.norm(i) for i in np.array(df[cols_T])]
    df_iso = Gaussian2004_iso_MHz_dataframe()
    if np.array(df['atom_name'] == df_iso['atom_name']).all():
        df['full_norm'] = np.array(
            [np.linalg.norm(np.array(df[cols_T])[i] + list(df_iso['aiso_MHz'])[i]) for i in range(len(df))])
    return df


Gaussian2004_axes = dict()
Gaussian2004_axes['N5'] = np.array([[0.9518, -0.3068, 0.0], [0.3068, 0.9518, 0.0], [0.0, 0.0, 1.0]])
Gaussian2004_axes['N10'] = np.array([[0.6845, 0.7291, 0], [0.7291, -0.6845, 0.0], [0.0, 0.0, 1.0]])
Gaussian2004_axes['NE1'] = np.array([[0.3221, 0.9353, -0.1463], [0.9173, -0.3466, -0.1963], [0.2343, 0.0710, 0.9696]])
Gaussian2004_axes['HE1'] = np.array([[0.7540, 0.6139, -0.2336], [0.2344, 0.0808, 0.9688], [-0.6136, 0.7853, 0.0830]])

Gaussian2004_axes["FN5"] = np.array([[0.9518, -0.3068, 0.], [0.3068, 0.9518, 0.], [0., 0., 1.]])
Gaussian2004_axes["FN10"] = np.array([[0.6845, 0.7291, 0.], [0.7291, -0.6845, 0.], [0., 0., 1.]])
Gaussian2004_axes["FN1"] = np.array([[-0.467, 0.8843, 0.], [0.8843, 0.467, 0.], [0., 0., 1.]])
Gaussian2004_axes["FN3"] = np.array([[0.8526, -0.5226, 0.], [0., 0., 1.], [0.5226, 0.8526, 0.]])
Gaussian2004_axes["FH3"] = np.array([[0., 0., 1.], [0.879, -0.4769, 0.], [0.4769, 0.879, 0.]])
Gaussian2004_axes["FH1'"] = np.array([[-0.3016, 0.0392, 0.9526], [0.5871, 0.7949, 0.1532], [0.7512, -0.6055, 0.2627]])
Gaussian2004_axes["FH1''"] = np.array(
    [[0.3016, -0.0392, 0.9526], [0.5871, 0.7949, -0.1532], [0.7512, -0.6055, -0.2627]])
Gaussian2004_axes["FH9"] = np.array([[0., 0., 1.], [0.9961, -0.0882, 0.], [0.0882, 0.9961, 0.]])
for idx in ['1', '2', '3']:  # Methyl group average
    Gaussian2004_axes["FH8" + idx] = np.identity(3)
    Gaussian2004_axes["FH7" + idx] = np.identity(3)
Gaussian2004_axes["FH6"] = np.array([[0.422, 0.9066, 0.], [0., 0., 1.], [0.9066, -0.422, 0.]])
Gaussian2004_axes["WNE1"] = np.array([[0.3221, 0.9353, -0.1463], [0.9173, -0.3466, -0.1963], [0.2343, 0.071, 0.9696]])
Gaussian2004_axes["WHE1"] = np.array([[0.754, 0.6139, -0.2336], [0.2344, 0.0808, 0.9688], [-0.6136, 0.7853, 0.083]])
Gaussian2004_axes["WHD1"] = np.array([[0.919, -0.3028, -0.2525], [0.2782, 0.0442, 0.9595], [0.2793, 0.952, -0.1249]])
Gaussian2004_axes["WHZ2"] = np.array([[0.5779, 0.7959, -0.1803], [0.2498, 0.0378, 0.9676], [0.7769, -0.6042, -0.1769]])
Gaussian2004_axes["WHH2"] = np.array([[-0.629, 0.7659, 0.1335], [0.2463, 0.0335, 0.9686], [0.7373, 0.6421, -0.2097]])
Gaussian2004_axes["WHZ3"] = np.array([[-0.552, 0.4129, 0.7245], [0.6965, -0.2494, 0.6728], [0.4585, 0.8759, -0.1499]])
Gaussian2004_axes["WHE3"] = np.array([[0.5359, 0.8279, -0.1653], [0.2657, 0.0205, 0.9638], [0.8014, -0.5604, -0.209]])
Gaussian2004_axes["WHB1"] = np.array([[0.2968, -0.3935, 0.8701], [0.818, 0.5749, -0.019], [-0.4927, 0.7174, 0.4925]])
Gaussian2004_axes["WHB2"] = np.array([[0.3333, 0.0783, 0.9396], [-0.243, 0.97, 0.0053], [0.911, 0.2301, -0.3423]])
Gaussian2004_axes["WHA"] = np.array([[-0.0934, 0.5118, 0.854], [0.9368, 0.3356, -0.0987], [-0.3371, 0.7908, -0.5108]])
Gaussian2004_axes["WHNa"] = np.array([[0.2189, 0.9709, -0.0971], [-0.6701, 0.2219, 0.7083], [0.7093, -0.0899, 0.6992]])
Gaussian2004_axes["WHNb"] = np.array([[-0.4432, 0.1417, 0.8852], [-0.2267, 0.9376, -0.2636], [0.8673, 0.3175, 0.3834]])
Gaussian2004_axes["WN"] = np.array([[-0.4082, 0.6826, 0.6062], [0.6009, 0.7008, -0.3844], [0.6872, -0.2073, 0.6962]])

Gaussian2004_iso_mT_from_MHz = dict(zip(Gaussian2004_iso_MHz.keys(),
                                        [MHz_to_mT(i) for i in Gaussian2004_iso_MHz.values()]))
Gaussian2004_anis_mT_from_MHz = dict(zip(Gaussian2004_anis_MHz.keys(),
                                         [MHz_to_mT(i) for i in Gaussian2004_anis_MHz.values()]))


def rotate_from_WPA_to_FADPA(HFC_WPA, Rwc, Rfc):
    if type(HFC_WPA) == str:
        hfc_w = W_HFCs_WPA(HFC_WPA)
    else:
        hfc_w = HFC_WPA
    hfc_c = np.linalg.inv(Rwc) @ hfc_w @ Rwc
    hfc_f = Rfc @ hfc_c @ np.linalg.inv(Rfc)
    return hfc_f


def HFCs_Gaussian2004_axis_system_MHz(atom_name):
    axes = Gaussian2004_axes[atom_name]
    iso = Gaussian2004_iso_MHz[atom_name]
    anis = Gaussian2004_anis_MHz[atom_name]
    return axes.T @ np.diag(iso + anis) @ axes


def HFCs_Gaussian2004_axis_system_mT(atom_name):
    return HFCs_Gaussian2004_axis_system_MHz(atom_name) * (
            1e3 / scipy.constants.physical_constants['electron gyromag. ratio over 2 pi'][0])


def FAD_HFCs_FADPA(atom_name):
    Rfg = Gaussian2004_FAD_rotation_matrix
    tensor_Gaussian = HFCs_Gaussian2004_axis_system_MHz(atom_name)
    return Rfg @ tensor_Gaussian @ Rfg.T


def W_HFCs_WPA(atom_name):
    if atom_name[0] != 'W' and atom_name != 'NE1':
        raise KeyError('Attempting to perform tryptophan rotations on non-Trp tensor!')

    def get_tensor(name):
        Rwg = Gaussian2004_W_rotation_matrix
        tensor_Gaussian = HFCs_Gaussian2004_axis_system_MHz(name)
        return Rwg @ tensor_Gaussian @ Rwg.T

    if atom_name != 'WHNm':
        return get_tensor(atom_name)
    else:
        return 0.5 * get_tensor('WHNa') + 0.5 * get_tensor('WHNb')


default_D_prefactor = (scipy.constants.mu_0 / (4 * np.pi)) * \
                      scipy.constants.physical_constants['Planck constant over 2 pi'][0] * (gyro_electron ** 2)


def get_DC_tensor_from_dipolar_vector_without_prefactor(dipolar_vector):
    r = dipolar_vector * 1e-10
    r_length = np.linalg.norm(r)
    return np.array(
        [[((r_length ** 2) * np.identity(3))[i, j] - 3 * r[i] * r[j] for i in range(3)] for j in range(3)]) / (
                   r_length ** 5)


def get_DC_tensor_from_dipolar_vector(dipolar_vector, D_prefactor=default_D_prefactor):
    return D_prefactor * get_DC_tensor_from_dipolar_vector_without_prefactor(dipolar_vector)


ClCry_crystal_WC_dipolar_vector = np.array([-9.561276096275549, -13.691239246983221, 5.540701853737576])
ClCry_crystal_WD_dipolar_vector = np.array([-9.13490867380175, -18.70182455744158, 4.29010566464781])
ClCry_crystal_WC_DC_tensor = get_DC_tensor_from_dipolar_vector(ClCry_crystal_WC_dipolar_vector)
ClCry_crystal_WD_DC_tensor = get_DC_tensor_from_dipolar_vector(ClCry_crystal_WD_dipolar_vector)

ClCry_crystal_Rfc = np.array([[-0.16589802507880655, 0.9440869552950132, -0.284916945997853],
                              [0.08311958567422897, -0.2745056082497105, -0.9579863284602765],
                              [-0.9826336956121015, -0.18261021844696496, -0.032932178282882314]])
ClCry_crystal_WC_Rwc = np.array([[0.7332285393631011, 0.012948875903588501, 0.6798589821987255],
                                 [0.008596141712293476, 0.9995622569522814, -0.028309023722545124],
                                 [-0.679927948690974, 0.02660114827023299, 0.7327962632953238]])
ClCry_crystal_WD_Rwc = np.array([[0.7133272281445452, 0.64932983854706, -0.2636949494398193],
                                 [-0.6979171990445918, 0.6924352441633543, -0.18287978542798936],
                                 [0.06384237515452737, 0.3144903709382608, 0.9471113755634781]])
ClCry_crystal_Rwc_dict = dict(zip(['WC', 'WD'], [ClCry_crystal_WC_Rwc, ClCry_crystal_WD_Rwc]))


def HFCs_residuePA(atom_name):
    if atom_name in FAD_hyperfine_available:
        return FAD_HFCs_FADPA(atom_name)
    elif atom_name in W_hyperfine_available:
        return W_HFCs_WPA(atom_name)


def ClCry_crystal_tensor(atom_name, W_label):
    Rwc = ClCry_crystal_Rwc_dict[W_label]
    return rotate_from_WPA_to_FADPA(atom_name, Rwc, ClCry_crystal_Rfc)


stored_HFCs = dict()
stored_HFCs['ClCry_crystal'] = ClCry_crystal_tensor
stored_HFCs['residuePA'] = HFCs_residuePA


def parse_tryptophan_atom_name(atom_name):
    if atom_name[:2] in ['WC', 'WD']:
        atom_key = 'W' + atom_name[2:]
        W_tag = atom_name[:2]
    else:
        atom_key = atom_name
        W_tag = None
    return atom_key, W_tag


def load_isotropic_coupling_constant(atom_name):
    atom_key, _ = parse_tryptophan_atom_name(atom_name)
    return Gaussian2004_iso_MHz[atom_key]


def load_tryptophan_hyperfine_coupling_tensor_from_key_and_tag(user_input, atom_key, W_tag):
    HFC_loader = stored_HFCs[user_input]
    if W_tag is not None:
        return HFC_loader(atom_key, W_tag)
    elif W_tag is None and user_input == 'residuePA':
        # Currently the only scenario in which which tryptophan it is does not matter
        return HFC_loader(atom_key)
    else:  # default to WC
        return HFC_loader(atom_key, 'WC')


def load_tryptophan_hyperfine_coupling_tensor(user_input, atom_name):
    atom_key, W_tag = parse_tryptophan_atom_name(atom_name)
    if atom_key != 'WHNm':
        return load_tryptophan_hyperfine_coupling_tensor_from_key_and_tag(user_input, atom_key, W_tag)
    else:
        return np.mean([load_tryptophan_hyperfine_coupling_tensor_from_key_and_tag(user_input, key, W_tag) for key in
                        ['WHNa', 'WHNb']], axis=0)
