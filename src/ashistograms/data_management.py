import os
import numpy as np
import abc


def parse_folder_path_input(user_input):
    if user_input == 'current_folder':
        user_input = os.getcwd()
    return user_input + '/'


def generate_anisotropic_result_loaders(folder_path, result_strings, load_all_if_exists=True, num_points=None,
                                        **kwargs):
    # Code will fail if result_strings is not passed;
    return [AnisotropicResultsLoader(folder_path, rs, load_all_if_exists=load_all_if_exists, num_points=num_points) for
            rs in result_strings]


class ResultLoader:
    def __init__(self, folder_path, result_string, num_files=None):
        self.path = parse_folder_path_input(folder_path)
        self.result_string = result_string
        self.num_files = num_files
        self.format_ind = None
        self.current = None
        self.is_bundled = self.check_if_bundled()
        self.all_results = None

    def check_if_bundled(self):
        return 'all' + self.result_string in os.listdir(self.path)

    def result_exists(self, ind):
        return self.is_bundled or os.path.exists(self.generate_dest(ind))

    def generate_filename(self, ind):
        return self.format_ind(ind) + self.result_string

    def set_current(self, ptind):
        self.current = self.load_result(ptind)
        return

    def get_missing_inds(self):
        return np.array([ind for ind in range(self.num_files) if not self.result_exists(ind)])

    def get_existing_inds(self):
        return np.array([ind for ind in range(self.num_files) if self.result_exists(ind)])

    def remove_result(self, ind):
        os.remove(self.generate_dest(ind))

    def generate_dest(self, ind):
        return self.path + self.generate_filename(ind)

    @abc.abstractmethod
    def load_result(self, ind):
        ...


class AnisotropicResultsLoader(ResultLoader):
    def __init__(self, folder_path, result_string, load_all_if_exists=True, num_points=None):
        '''current_folder available as an argument for folder_path.'''
        ResultLoader.__init__(self, folder_path, result_string, num_files=num_points)
        self.num_points = self.num_files
        self.format_ind = lambda ptind: 'ptind_%d' % (ptind)
        self.all_arr_path = self.path + 'all' + self.result_string
        self.store_num_points(num_points)
        if load_all_if_exists:
            self.load_all()
        return

    def load_all(self):
        if self.is_bundled:
            self.all_results = np.load(self.all_arr_path)
        return

    def store_num_points(self, num_points):
        if type(num_points) == int:
            self.num_points = num_points

    def remove_result(self, ptind):
        os.remove(self.generate_ptind_dest(ptind))

    def generate_ptind_dest(self, ptind):
        return self.path + self.generate_filename(ptind)

    def calculate_and_save_all(self, calculate_ptind, num_points='provided', attempt_bundling=True,
                               overwrite_if_exists=False):
        self.store_num_points(num_points)
        for ptind in range(self.num_points):
            self.current = calculate_ptind(ptind)
            self.save_current(ptind, overwrite_if_exists=overwrite_if_exists)
        if attempt_bundling:
            self.attempt_bundling(self.num_points)

    def save_current(self, ptind, overwrite_if_exists=False):
        already_exists = self.result_exists(ptind)
        if not already_exists or overwrite_if_exists:
            np.save(self.generate_ptind_dest(ptind), self.current)
        return

    def load_unbundled_result(self, ptind):
        return np.load(self.generate_ptind_dest(ptind))

    def load_result(self, ptind):
        if self.is_bundled:
            return self.all_results[ptind]
        else:
            return self.load_unbundled_result(ptind)

    def attempt_bundling(self, num_points='provided'):
        self.store_num_points(num_points)
        self.is_bundled = self.check_if_bundled()
        if not self.is_bundled:
            exists = np.array([self.result_exists(i) for i in range(self.num_points)])
            if exists.all():
                self.all_results = []
                for ptind in range(self.num_points):
                    self.all_results.append(self.load_unbundled_result(ptind))
                np.save(self.all_arr_path, np.array(self.all_results))
                self.is_bundled = True
                for ptind in range(self.num_points):
                    os.remove(self.generate_ptind_dest(ptind))
                return
            else:
                missing = np.arange(self.num_points)[~exists]
                return missing

    def get_mean(self):
        return np.mean(self.all_results, axis=0)
