import numpy as np
import os
import ashistograms as ash
import ashistograms.defaults as defaults
field_directions = np.loadtxt('field_directions.txt')
J_MHz = 0
DC_tensor = ash.ClCry_crystal_WC_DC_tensor

class Diagonaliser:
    def __init__(self, num_nuclei):
        nucspins = ash.defaults.generate_nuclear_spinsys(num_nuclei)
        output_path = nucspins.name
        os.makedirs(output_path, exist_ok = True)
        self.eigenvalues, self.Smats = ash.generate_anisotropic_result_loaders(output_path, ash.eigenspace_results_strings, num_points = len(field_directions))
        self.radical_pair = ash.RadicalPair(nucspins, J_MHz = J_MHz, DC_tensor=DC_tensor)

    def diagonalise_radical_pair_hamiltonian(self,ptind, sparse):
        if not self.Smats.result_exists(ptind):
            theta, phi = field_directions[ptind]
            self.eigenvalues.current, self.Smats.current = self.radical_pair.get_eigH(theta, phi, sparse)
            self.eigenvalues.save_current(ptind)
            self.Smats.save_current(ptind)
            del self.eigenvalues.current, self.Smats.current
        else:
            return 'exists'
    
    def bundle_all(self):
        for result in [self.eigenvalues, self.Smats]:
            result.attempt_bundling()

if __name__ == '__main__':
    import argparse
    from datetime import datetime

    parser = argparse.ArgumentParser(
        description='Diagonalises the hamiltonian for a coupled FAD-TRP radical pair.')
    parser.add_argument('num_nuclei', metavar='NUM_NUCLEI', type=int, nargs=1, help='Total number of nuclei')
    parser.add_argument('start_ptind', metavar='START_POINT_INDEX', type=int, nargs='?', default=0,
                        help='Direction point index to start at')
    parser.add_argument('num_pts_to_run', metavar='NUM_POINTS_TO_RUN', type=int, nargs='?', default=5,
                        help='Number of direction points to run')
#     parser.add_argument('--dense', dest ='use_sparse', action='store_const', const = False, default = True, help='Use dense matrices instead of sparse matrices (default). Recommended for small systems.')
    parser.add_argument('--all', dest='run_all', action='store_const', const=True, default=False,
                        help='Run all currently missing directions, then bundle and normalise histogram heights, ignore user inputs of starting point and num points to run.')
    parser.add_argument('--bund', dest='bundle_all', action='store_const', const=True, default=False,
                        help='Bundle arrays of eigenvalues and matrix of eigenvectors (Smatrix) for all direcions into one big file. Only use for small spin systems.')
    parser.add_argument('--s', dest='silent', action='store_const', const=True, default=False,
                        help='Silent mode: disables printing of timestamps in the terminal')

    user_input = parser.parse_args()

    calculator = Diagonaliser(user_input.num_nuclei[0])

    if user_input.run_all:
        ptinds = np.arange(len(field_directions))
    else:
        end_ptind = min(len(field_directions), user_input.start_ptind + user_input.num_pts_to_run)
        ptinds = np.arange(user_input.start_ptind, end_ptind)

    if not user_input.silent:
        print(datetime.now(), 'Starting ... ')

    for ptind in ptinds:
        calculated = calculator.diagonalise_radical_pair_hamiltonian(ptind,sparse=False) is None
        if not user_input.silent and calculated:
            print(datetime.now(), 'Finished: point index %d' % (ptind))
            
    if user_input.bundle_all:
        calculator.bundle_all()

    if not user_input.silent:
        print(datetime.now(), 'Done.')
