import numpy as np
import matplotlib.pyplot as plt
import ashistograms as ash
import ashistograms.defaults
bin_edges = ashistograms.defaults.bin_edges

if __name__ == '__main__':

    import argparse
    from datetime import datetime

    parser = argparse.ArgumentParser(
        description='Plots the calculation results for the action spectrum histogram of a coupled FAD-TRP radical pair.')
    parser.add_argument('num_nuclei', metavar='NUM_NUCLEI', type=int, nargs=1, help='Total number of nuclei')
    user_input = parser.parse_args()
    
    folder = ash.defaults.generate_nuclear_spinsys(user_input.num_nuclei[0]).name    

    bin_midpoints = 0.5*(bin_edges[:-1]+bin_edges[1:])
    mean_heights = np.load(f'{folder}/all_normalised_heights.npy').mean(axis=0)

    fig,ax = plt.subplots(figsize=(10,6))
    fontsize = 20
    ax.bar(bin_midpoints, mean_heights,facecolor = 'teal', width = bin_edges[1]-bin_edges[0])
    ax.set_xlim(0,120)
    ax.set_ylabel('Resonance effect',fontsize=fontsize)
    ax.set_xlabel('Frequency / MHz',fontsize=fontsize)
    ax.tick_params(labelsize=fontsize*0.8, width = 2, length = 10)
    for side in ['left','bottom']:
        ax.spines[side].set_linewidth(2)
    for side in ['top','right']:
        ax.spines[side].set_linewidth(0)
    plt.tight_layout()
    fig.savefig(f'{folder}/histogram.jpg')
    plt.show()
