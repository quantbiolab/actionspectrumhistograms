import numpy as np
import os
import ashistograms as ash
import ashistograms.defaults as defaults

field_directions = np.loadtxt('field_directions.txt')
J_MHz = 0
DC_tensor = ash.ClCry_crystal_WC_DC_tensor


def generate_calculator(num_nuclei):
    nuclear_spinsys = ash.defaults.generate_nuclear_spinsys(num_nuclei)
    calculator = ash.defaults.generate_calculator_for_coupled_radical_pair(nuclear_spinsys, field_directions)
    calculator.rp.set_up_radical_pair(J_MHz = J_MHz, DC_tensor=DC_tensor)
    return calculator

if __name__ == '__main__':

    import argparse
    from datetime import datetime

    parser = argparse.ArgumentParser(
        description='Runs the histogram calculation for a coupled FAD-TRP radical pair.')
    parser.add_argument('num_nuclei', metavar='NUM_NUCLEI', type=int, nargs=1, help='Total number of nuclei')
    parser.add_argument('start_ptind', metavar='START_POINT_INDEX', type=int, nargs='?', default=0,
                        help='Direction point index to start at')
    parser.add_argument('num_pts_to_run', metavar='NUM_POINTS_TO_RUN', type=int, nargs='?', default=5,
                        help='Number of direction points to run')
    parser.add_argument('--all', dest='run_all', action='store_const', const=True, default=False,
                        help='Run all currently missing directions, then bundle and normalise histogram heights, ignore user inputs of starting point and num points to run.')
    parser.add_argument('--norm', dest='normalise_heights', action='store_const', const=True, default=False,
                        help='Normalise and bundle histogram heights.')
    parser.add_argument('--bund', dest='bundle_all', action='store_const', const=True, default=False,
                        help='Bundle histogram heights and parameters other than heights (energy gaps, resonance effects etc.)')
    parser.add_argument('--s', dest='silent', action='store_const', const=True, default=False,
                        help='Silent mode: disables printing of timestamps in the terminal')

    user_input = parser.parse_args()    

    calculator = generate_calculator(user_input.num_nuclei[0])

    if user_input.run_all:
        ptinds = np.arange(len(field_directions))
        user_input.normalise_heights = True
    else:
        end_ptind = min(len(field_directions), user_input.start_ptind + user_input.num_pts_to_run)
        ptinds = np.arange(user_input.start_ptind, end_ptind)

    if not user_input.silent:
        print(datetime.now(), 'Starting ... ')

    for ptind in ptinds:
        calculated = calculator.calculate_if_absent(ptind) is None
        if not user_input.silent and calculated:
            print(datetime.now(), 'Finished: point index %d' % (ptind))

    if user_input.normalise_heights or user_input.bundle_all:
        calculator.normalise_all_bar_heights()

    if user_input.bundle_all:
        calculator.bundle_non_height_results()
        
    if not user_input.silent:
        print(datetime.now(), 'Done.')
