# Action Spectrum Histograms

Python code to calculate action-spectrum histograms, accompanying the paper "Upper bound for broadband radiofrequency field disruption of magnetic compass orientation in night-migratory songbirds" by Leberecht et al.

The package can be installed by running "pip install ." in this directory. Example scripts are provided in example_runscripts.
