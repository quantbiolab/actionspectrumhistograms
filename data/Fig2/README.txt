The histogram bar heights provided here correspond to a bin width of 0.1 MHz. To generate the histograms shown in Figure 2, every 5 histogram bars are summed to give a bin width of 0.5 MHz.
